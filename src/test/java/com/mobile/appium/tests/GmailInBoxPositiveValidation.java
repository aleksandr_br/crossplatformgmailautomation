package com.mobile.appium.tests;

import com.mobile.appium.page.LoginEmailPage;
import com.mobile.appium.page.LoginPasswordPage;
import com.mobile.appium.page.MainPage;
import com.mobile.appium.report.Reporter;
import com.mobile.appium.utils.Assertion;
import com.mobile.appium.utils.ScreenShotCapture;
import org.testng.annotations.Test;

import java.util.UUID;

public class GmailInBoxPositiveValidation extends BaseTest {

    @Test(description = "Verify that when you sent message to someone the message in present in inbox")
    public void test_ID1_sendMessageAndCheckInboxTest() {
        String random = UUID.randomUUID().toString();
        Reporter.logStep("Step 1 : Login to Gmail");
        LoginEmailPage page = new LoginEmailPage();
        page.fillEmailField(USER_FIRST);
        page.clickSubmitButton();
        LoginPasswordPage passPage = new LoginPasswordPage();
        ScreenShotCapture.getscreenshot();
        passPage.fillPasswordFld(PASS);
        passPage.clickSubmitButton();
        MainPage mainPage = new MainPage();
        Assertion.verifyTrue(mainPage.isLogoPresent(), "Verify That user logged in and the LOGO is present");

        Reporter.logStep("Step 2 : Send Msg");
        mainPage.clickComposeBtn();
        Assertion.verifyTrue(mainPage.isMessageFormPresent(), "Verify that POST msg form is present");
        Reporter.pageView("POST Form View");
        mainPage.fillAndSendMsgForm(USER_SECOND, random, random);
        Assertion.verifyTrue(mainPage.isSendSuccess(), "Verify that the message was sent");
        Reporter.pageView("Sent Msg Alert View");

        Reporter.logStep("Step 3 : LogOut");
        mainPage.jsClick(mainPage.logoBtn);
        Assertion.verifyTrue(mainPage.isAvatarPresent(), "Verify that Avatar displayed");
        Reporter.pageView("Avatar View");
        mainPage.logOut();
        Reporter.pageView("Logout View");

        Reporter.logStep("Step 4 : Log In to Other Account");
        mainPage.switchTheAccount();
        Reporter.pageView("Switch View");
        page = new LoginEmailPage();
        page.fillEmailField(USER_SECOND);
        page.clickSubmitButton();
        passPage = new LoginPasswordPage();
        passPage.fillPasswordFld(PASS);
        passPage.clickSubmitButton();
        mainPage = new MainPage();
        Assertion.verifyTrue(mainPage.isMsgPresent(USER_FIRST, random, random));
        Reporter.pageView("In-Box View");
    }

}
