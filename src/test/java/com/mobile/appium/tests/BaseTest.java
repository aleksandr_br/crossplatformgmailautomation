package com.mobile.appium.tests;

import com.mobile.appium.driver.DriverFactory;
import com.mobile.appium.driver.DriverType;
import com.mobile.appium.listener.CustomListener;
import com.mobile.appium.property.PropertyProvider;
import com.mobile.appium.report.ExtentTestManager;
import com.mobile.appium.report.Reporter;
import com.mobile.appium.utils.MailAPI;
import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.*;

@Listeners(CustomListener.class)
public abstract class BaseTest {

    private WebDriver driver;
    static final String PASS = PropertyProvider.getProperty("user_data", "password");
    static final String USER_FIRST = PropertyProvider.getProperty("user_data", "user_first");
    static final String USER_SECOND = PropertyProvider.getProperty("user_data", "user_second");
    private static final String SENT_FOLDER = "[Gmail]/Sent Mail";
    private static final String INBOX_FOLDER = "inbox";


    @Parameters({"platform", "browser"})
    @BeforeMethod(alwaysRun = true)
    public void setup(String platform, String browser){
        DriverType.getINSTANCE().setType(platform);
        if (platform.equalsIgnoreCase("MOBILE")){
            driver = (AndroidDriver) DriverFactory.MOBILE.getDriver(browser);
        }
        else {
            driver = DriverFactory.DESKTOP.getDriver(browser);
        }
        driver.get("http://www.gmail.com");
    }

    @AfterMethod(alwaysRun = true)
    public void tearDown(){
        Reporter.info("Close browser");
        driver.close();
    }

    @AfterSuite(alwaysRun = true)
    public void afterSuite() {
        MailAPI.deleteAllMailIn(USER_FIRST, PASS, SENT_FOLDER);
        MailAPI.deleteAllMailIn(USER_SECOND, PASS, INBOX_FOLDER);
        ExtentTestManager.close();
    }

}
