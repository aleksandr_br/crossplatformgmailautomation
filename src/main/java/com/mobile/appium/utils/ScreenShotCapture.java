package com.mobile.appium.utils;

import com.mobile.appium.driver.DriverFactory;
import com.mobile.appium.driver.DriverType;
import com.mobile.appium.property.PropertyProvider;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.testng.TestNG;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

public class ScreenShotCapture {

    public static String getscreenshot() {
        try {
            boolean isDesktop = DriverType.getINSTANCE().getType().equalsIgnoreCase("DESKTOP");
            WebDriver driver = isDesktop ? DriverFactory.DESKTOP.getDriver() : DriverFactory.MOBILE.getDriver();
            String screen = PropertyProvider.getProperty("desktop_config", "report_dir") + "screenShot//" + LocalDateTime.now().toString().replaceAll(":", "-") + ".jpeg";
            File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(scrFile, new File(screen));
            return System.getProperty("user.dir") + "/" + screen;
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }

    }


}
