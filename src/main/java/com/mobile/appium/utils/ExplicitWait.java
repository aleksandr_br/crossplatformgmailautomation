package com.mobile.appium.utils;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ExplicitWait {
    private final WebDriver driverWait;
    private final Wait<WebDriver> wait;

    public ExplicitWait(WebDriver driver) {
        this.driverWait = driver;
        wait = new WebDriverWait(this.driverWait, 15)
                .ignoring(StaleElementReferenceException.class, WebDriverException.class)
                .withMessage("Element was not found by locator ");
    }

    public void waitForElementIsClickable(WebElement element) {
        try{
            wait.until(ExpectedConditions.elementToBeClickable(element));
        }
        catch(RuntimeException e){
            throw new RuntimeException("Element wasn't found");
        }
    }

    public void isElementPresent(WebElement locator){
        try{
            new WebDriverWait(driverWait, 3).until(ExpectedConditions.and(ExpectedConditions.visibilityOf(locator)));
        }
        catch(RuntimeException e){
            throw new RuntimeException("Element isn't on the page");
        }
    }


}
