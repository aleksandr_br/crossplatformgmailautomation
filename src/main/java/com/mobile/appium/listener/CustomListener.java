package com.mobile.appium.listener;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

import com.mobile.appium.report.ExtentManager;
import com.mobile.appium.report.ExtentTestManager;
import com.mobile.appium.report.Reporter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class CustomListener implements ITestListener{

    private Logger log = LogManager.getLogger();

    @Override
    public void onTestStart(ITestResult result) {
        log.info("Test start running:"  + result.getMethod().getMethodName() + " at:" + LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_TIME));
        ExtentTestManager.startTest(result.getMethod().getMethodName(), result.getMethod().getDescription());
    }
    @Override
    public void onTestSuccess(ITestResult result) {
        log.info(result.getMethod().getMethodName() + "Success");
        Reporter.testPassLog();
        ExtentTestManager.endTest();
    }
    @Override
    public void onTestFailure(ITestResult result) {
        Reporter.testFailLog(result.getThrowable().getMessage() + "\n\n" + Arrays.toString(result.getThrowable().getStackTrace()));
        ExtentTestManager.endTest();
        result.getThrowable().printStackTrace();
    }
    @Override
    public void onTestSkipped(ITestResult result) {
        Reporter.testSkipLog();
        ExtentTestManager.endTest();
    }
    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        onTestFailure(result);
    }
    @Override
    public void onStart(ITestContext context) {
        log.info("Start Execute Automation Tests");
    }
    @Override
    public void onFinish(ITestContext context) {
        log.info("Passed: " + context.getPassedTests().size());
        log.info("Failed:" + context.getFailedTests().size());
        log.info("Find the Extend Report Here " + ExtentManager.getLocation());
    }
}