package com.mobile.appium.page;

import com.mobile.appium.driver.DriverFactory;
import com.mobile.appium.report.Reporter;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class LoginPasswordPage extends BasicPage{
    @FindBy(name = "password")
    WebElement passFld;
    @FindBy(id = "passwordNext")
    WebElement nextBtn;

    public LoginPasswordPage() {
        super();
    }

    public void fillPasswordFld(String value){
        Reporter.info(String.format("Start to fill password field with %s", value));
        wait.waitForElementIsClickable(passFld);
        passFld.click();
        passFld.clear();
        passFld.sendKeys(value);
        passFld.submit();
    }

    public void clickSubmitButton(){
        wait.waitForElementIsClickable(nextBtn);
        nextBtn.click();
    }
}
