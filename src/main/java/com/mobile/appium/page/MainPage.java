package com.mobile.appium.page;

import com.mobile.appium.driver.DriverFactory;
import com.mobile.appium.driver.DriverType;
import com.mobile.appium.property.PropertyProvider;
import com.mobile.appium.report.Reporter;
import com.mobile.appium.utils.ScreenShotCapture;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidBy;
import io.appium.java_client.pagefactory.AndroidFindAll;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

import java.util.List;

public class MainPage extends BasicPage {

    @FindBy(xpath = "//*[contains(@title, 'Google Account')] | //*[@id = 'tltbt']/child::*[2]/child::*[1] | //*[@text = 'Menu']")
    public  WebElement logoBtn;
    @FindBy(xpath = "//textarea[@name = 'to'] | //*[@id = 'composeto']")
    private WebElement toFld;
    @FindBy(xpath = "//input[@placeholder = 'Subject'] | //*[@id = 'cmcsubj']")
    private WebElement subjectFld;
    @FindBy(xpath = "//div[@aria-label = 'Message Body'] | //*[@id = 'cmcbody']")
    private WebElement messageFld;
    @FindBy(xpath = "//div[@class = 'J-J5-Ji btA']/div[@role = 'button'] | //*[@id = 'cvtbt']/child::*[4]")
    private WebElement sendBtn;
    @FindBy(xpath = "//*[@id = 'link_vsm'] | //*[@id = 'tltbt']/child::*[4]")
    private WebElement viewMsgBtn;
    @FindBy(xpath = "//div[@aria-label = 'Account Information']//*[@title = 'Profile'] | //*[@id = 'mutbt']/child::*[3]/child::*[3]")
    private WebElement avatarPct;
    @FindBy(xpath = "//div[@aria-label = 'Account Information']//a[@id = 'gb_71'] | //*[@id = 'mutbt']/child::*[3]/child::*[3]")
    private WebElement logoutBtn;
    @FindBy(xpath = "//div[@role = 'dialog'] | //*[@id = 'cvtbt']/child::*[3]")
    private WebElement msgTbl;
    @FindBy(xpath = "//div[@gh = 'cm'] | //*[@id = 'tltbt']/child::*[4]")
    private WebElement composeBtn;
    @FindBy(xpath = "//*[@aria-label = 'Switch account']")
    private WebElement switchAccount;

    public MainPage() {
        super();
    }

    public boolean isLogoPresent() {
        if (isDesktop) {
            wait.isElementPresent(logoBtn);
        } else {
            slideToElement(logoBtn);
        }
        Reporter.pageView("Page with LOGO view");
        return logoBtn.isEnabled();
    }

    public boolean isMessageFormPresent() {
        return true;
    }

    public void fillAndSendMsgForm(String email, String subject, String msg) {
        Reporter.info(String.format("Start to fill message post field \nemail: %s\nsubject: %s\nmessage: %s ", email, subject, msg));
        wait.waitForElementIsClickable(toFld);
        toFld.click();
        toFld.clear();
        toFld.sendKeys(email);
        msgTbl.click();
        wait.waitForElementIsClickable(subjectFld);
        subjectFld.click();
        subjectFld.clear();
        subjectFld.sendKeys(subject);
        msgTbl.click();
        wait.waitForElementIsClickable(messageFld);
        messageFld.click();
        messageFld.clear();
        messageFld.sendKeys(msg);
        slideToElement(sendBtn);
        wait.waitForElementIsClickable(sendBtn);
        sendBtn.click();
    }

    public boolean isSendSuccess() {
        wait.waitForElementIsClickable(viewMsgBtn);
        return viewMsgBtn.isEnabled();
    }

    public boolean isAvatarPresent() {
        wait.waitForElementIsClickable(avatarPct);
        return avatarPct.isDisplayed();
    }

    public void logOut() {
        wait.waitForElementIsClickable(logoutBtn);
        logoutBtn.click();
        if (isDesktop) {
            wait.waitForElementIsClickable(switchAccount);
            switchAccount.click();
        }
    }

    public void clickComposeBtn() {
        wait.waitForElementIsClickable(composeBtn);
        composeBtn.click();
    }

    public boolean isMsgPresent(String email, String subject, String msgTxt) {
        Reporter.info(String.format("Try to find message with subject: %s message: %s ", subject, msgTxt));
        if (!isDesktop) {
            String xpath = "//*[contains(@id , 'tl_')]/child::*[1]/child::*/child::*[7]/child::*";
            List<WebElement> elements = driver.findElements(By.xpath(xpath));
            for (WebElement element : elements) {
                jsGetText(element);
                if (element.getAttribute("textContent").contains(subject) && element.getAttribute("textContent").contains(msgTxt)) {
                    return true;
                }

            }
            return false;
        } else {
            String shortMsg = String.format("%s - %s", subject, msgTxt);
            String xpath = String.format("//tr/td/div[@class = 'yW']/span[@email = '%s']/../../../td[contains(@class, 'a4W')]", email.toLowerCase());
            WebElement msg = driver.findElements(By.xpath(xpath)).get(0);
            return msg.getText().equalsIgnoreCase(shortMsg);
        }

    }
}
