package com.mobile.appium.page;

import com.mobile.appium.driver.DriverFactory;
import com.mobile.appium.report.Reporter;
import io.appium.java_client.pagefactory.AndroidFindBy;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class LoginEmailPage extends BasicPage {

    @FindBy( id = "identifierId")
    WebElement emailFld;

    @FindBy( id = "identifierNext")
    WebElement nextBtn;

    public LoginEmailPage() {
        super();
    }

    public void fillEmailField(String value){
        Reporter.info(String.format("Start to fill email field with %s", value));
        wait.waitForElementIsClickable(emailFld);
        emailFld.click();
        emailFld.clear();
        emailFld.sendKeys(value);
        emailFld.submit();
    }

    public void clickSubmitButton(){
        wait.waitForElementIsClickable(nextBtn);
        nextBtn.click();
    }


}
