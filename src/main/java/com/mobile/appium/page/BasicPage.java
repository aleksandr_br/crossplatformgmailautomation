package com.mobile.appium.page;

import com.mobile.appium.driver.DriverFactory;
import com.mobile.appium.driver.DriverType;
import com.mobile.appium.report.Reporter;
import com.mobile.appium.utils.ExplicitWait;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

public abstract class BasicPage {
    protected ExplicitWait wait;
    protected WebDriver driver;
    boolean isDesktop;

    @FindBy(xpath = "//*[@id = 'account-list']/child::*[2] | //div[@role = 'button' and contains(@class , 'bLzI3e')]")
    WebElement addAccountButton;

    public BasicPage() {
        isDesktop = DriverType.getINSTANCE().getType().equalsIgnoreCase("DESKTOP");
        if (isDesktop) {
            driver = (WebDriver) DriverFactory.DESKTOP.getDriver();
        } else {
            driver = (AppiumDriver<MobileElement>) DriverFactory.MOBILE.getDriver();
        }
        PageFactory.initElements(new AppiumFieldDecorator(driver, 10, TimeUnit.SECONDS), this);
        wait = new ExplicitWait(driver);
    }

    public void slideToElement(WebElement toElement) {
        try {
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", toElement);
        } catch (Exception f) {
            Reporter.testFailLog(String.format("Crash in time of scrolling at point: [%s , %s]",toElement.getLocation().getX(), toElement.getLocation().getY()));
        }
    }

    public void jsClick(WebElement toElement) {
        try {
            ((JavascriptExecutor) driver).executeScript("arguments[0].click();", toElement);
        } catch (Exception f) {
            Reporter.testFailLog(String.format("Crash in time of clicking at point: [%s , %s]",toElement.getLocation().getX(), toElement.getLocation().getY()));
        }
    }

    public String jsGetText(WebElement toElement) {
        try {
            ((JavascriptExecutor) driver).executeScript("arguments[0].style.border='3px solid red'", toElement);
            return (String) ((JavascriptExecutor) driver).executeScript("return arguments[0].textContent;", toElement);
        } catch (Exception f) {
            Reporter.testFailLog(String.format("Crash in time of getting text at point: [%s , %s]",toElement.getLocation().getX(), toElement.getLocation().getY()));
            throw new RuntimeException("Crash in time of getting text ");
        }
    }

    public void switchTheAccount(){
        wait.waitForElementIsClickable(addAccountButton);
        if (!isDesktop){
            addAccountButton.click();
            return;
        }
        jsClick(addAccountButton);
    }
}
