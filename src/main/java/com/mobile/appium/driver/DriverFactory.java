package com.mobile.appium.driver;

import io.appium.java_client.AppiumDriver;
import org.openqa.selenium.WebDriver;

import java.util.HashMap;
import java.util.Map;

public enum DriverFactory {

    DESKTOP("DESKTOP"), MOBILE("MOBILE");

    private static Map<Long, WebDriver> driver;

    private final String type;

    DriverFactory(String type) {
        this.type = type;
    }


    public <T> T getDriver(String browser) {
        switch (this) {
            case DESKTOP:
                driver.put(Thread.currentThread().getId(), createDesktopDriver(browser));
                return (T) driver.get(Thread.currentThread().getId());
            case MOBILE:
                driver.put(Thread.currentThread().getId(), createMobileDriver(browser));
                return (T) driver.get(Thread.currentThread().getId());
            default:
                throw new RuntimeException("Wrong Platform Name");
        }
    }

    public <T> T getDriver() {
        if (!driver.isEmpty()) {
            return (T) driver.get(Thread.currentThread().getId());
        } else {
            return getDriver("Chrome");
        }
    }

    private AppiumDriver<?> createMobileDriver(String browser) {
        return MobileDriverFactory.getMobileBrowser();
    }

    private WebDriver createDesktopDriver(String browser) {
        return DesktopDriverFactory.getDriver(browser);
    }

    public String getType() {
        return type;
    }

    static {
        driver = new HashMap<>();
    }
}
