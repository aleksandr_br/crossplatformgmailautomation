package com.mobile.appium.driver;

import java.util.HashMap;
import java.util.Map;

public class DriverType {

    private static Map<Long, String> type;

    private DriverType() {
    }

    private final static DriverType INSTANCE = new DriverType();

    public static DriverType getINSTANCE() {
        return INSTANCE;
    }

    public String getType() {
        return type.get(Thread.currentThread().getId());
    }

    public void setType(String type) {
        this.type.put(Thread.currentThread().getId(), type);
    }

    static {
        type = new HashMap<>();
    }
}
