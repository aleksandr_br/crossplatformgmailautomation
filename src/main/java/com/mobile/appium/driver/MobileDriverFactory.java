package com.mobile.appium.driver;

import com.mobile.appium.property.PropertyProvider;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

class MobileDriverFactory {
    private static final long WAIT_SEC = Integer.parseInt(PropertyProvider.getProperty("mobile_config","newCommandTimeout"));
    private static AppiumDriver<?> driver;

    static AppiumDriver<?> getMobileBrowser(){
        final DesiredCapabilities capabilities = new DesiredCapabilities();
        final URL url;

        capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, PropertyProvider.getProperty("mobile_config", "deviceName"));
        capabilities.setCapability(MobileCapabilityType.UDID, PropertyProvider.getProperty("mobile_config", "udid"));
        capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, PropertyProvider.getProperty("mobile_config", "platformName"));
        capabilities.setCapability(MobileCapabilityType.VERSION, PropertyProvider.getProperty("mobile_config", "platformVersion"));
        capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, PropertyProvider.getProperty("mobile_config", "browserName"));
        capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, PropertyProvider.getProperty("mobile_config", "browserName"));
        capabilities.setCapability(MobileCapabilityType.FULL_RESET, "false");
        capabilities.setCapability("clearSystemFiles", "true");
        if (PropertyProvider.getProperty("mobile_config", "useAvd").equals("true")) {
            capabilities.setCapability("avd", PropertyProvider.getProperty("mobile_config", "avd"));
        }
        try {
            url = new URL(PropertyProvider.getProperty("mobile_config", "driverURL"));
            driver = new AndroidDriver<MobileElement>(url, capabilities);
            driver.manage().timeouts().implicitlyWait(WAIT_SEC, TimeUnit.SECONDS);
            driver.manage().timeouts().pageLoadTimeout(WAIT_SEC, TimeUnit.SECONDS);
        } catch (MalformedURLException e) {
            throw new RuntimeException("Error is raised during MOBILE driver creation");
        }
        return driver;
    }
}
