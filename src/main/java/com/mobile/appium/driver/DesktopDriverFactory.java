package com.mobile.appium.driver;

import com.mobile.appium.property.PropertyProvider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

public class DesktopDriverFactory {

    private static Logger log = LogManager.getLogger();
    private static Map<String, Supplier<WebDriver>> driverMap;
    private static Map<Long, WebDriver> driverThreadMap;

    public static WebDriver getDriver(String browser) {
        WebDriver driver;
        if (driverThreadMap.size() <= 2) {
            driver = driverMap.get(browser).get();
            driver.manage().timeouts().implicitlyWait(25, TimeUnit.SECONDS);
            driver.manage().timeouts().pageLoadTimeout(25, TimeUnit.SECONDS);
            driver.manage().timeouts().setScriptTimeout(25, TimeUnit.SECONDS);
            driver.manage().window().maximize();
        }
        else{
            throw new RuntimeException("No Empty Space For Driver Create");
        }
        return driver;
    }

    public static WebDriver getDriver() {
        return driverThreadMap.get(getId()) != null ? driverThreadMap.get(getId()) : getDriver("chrome");
    }

    private static WebDriver createFireFoxDriver() {
        System.setProperty(PropertyProvider.getProperty("desktop_config","gecko"), PropertyProvider.getProperty("desktop_config","gecko_path"));
        log.info("Opening FireFox Browser");
        driverThreadMap.put(getId(), new FirefoxDriver());
        return driverThreadMap.get(getId());
    }

    //Have TO ADD chromedriver if you want to work with it
    private static WebDriver createChromeDriver() {
        System.setProperty(PropertyProvider.getProperty("desktop_config","chrome"), PropertyProvider.getProperty("desktop_config","chrome_path"));
        log.info("Opening CHROME Browser");
        driverThreadMap.put(getId(), new ChromeDriver());
        return driverThreadMap.get(getId());
    }

    private static Long getId(){
        return Thread.currentThread().getId();
    }

    static {
        driverMap = new HashMap<>();
        driverThreadMap = new HashMap<>();
        driverMap.put("chrome", () -> createChromeDriver());
        driverMap.put("firefox", () -> createFireFoxDriver());
    }

}
