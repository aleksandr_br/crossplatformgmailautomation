package com.mobile.appium.report;

import com.mobile.appium.property.PropertyProvider;
import com.relevantcodes.extentreports.ExtentReports;

import java.time.LocalDateTime;


public class ExtentManager {

	private static final String reportFile = PropertyProvider.getProperty("desktop_config", "report_dir") + LocalDateTime.now().toString().replaceAll(":", "-") + ".html";
	private static final ExtentReports Instance = new ExtentReports(reportFile, true);
	
	private ExtentManager(){
	}
	
	static ExtentReports getInstance(){
		return Instance;
	}
	
	public static String getLocation(){
		return System.getProperty("user.dir") + "/" + reportFile;
	}
	
}
