package com.mobile.appium.report;

import java.util.HashMap;
import java.util.Map;

import com.mobile.appium.driver.DriverType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class ExtentTestManager { 
	
    private static ExtentReports extent = ExtentManager.getInstance();
    private static Map<Long, ExtentTest> testMap;
    private static Logger log  = LogManager.getLogger();
    

    public static ExtentTest getTest() {
        return testMap.get(getId());
    }

    public static void endTest() {
        extent.endTest(testMap.get(getId()));
    }

    public static ExtentTest startTest(String testName) {
        return startTest(testName, "");
    }

    public static ExtentTest startTest(String testName, String desc) {
    	testMap.put(getId(), 
    	extent.startTest(testName, String.format("[Description: %s (PLATFORM: %s)]", desc, DriverType.getINSTANCE().getType().toUpperCase())));
        return testMap.get(getId());
    }
    
    public static void close() {
		log.info("Close Extend Report");
    	extent.flush();
    }
    
    private static Long getId(){
    	return Thread.currentThread().getId();
    }
    
	static {
		testMap = new HashMap<>();
	}
}
