package com.mobile.appium.property;

import java.util.ResourceBundle;

public class PropertyProvider {
    public static String getProperty(String fileName, String key){
        return ResourceBundle.getBundle(fileName).getString(key);
    }
}
